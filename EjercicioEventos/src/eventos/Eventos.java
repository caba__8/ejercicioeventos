package eventos;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.*;

public class Eventos extends JFrame {
	public JPanel panel;
	public JLabel etiqueta;
	public JButton boton,boton1,boton2;
	
	public Eventos() {
		setVentana();
		init();
	}

	private void setVentana() {
		setSize(400,400);
		setTitle("Eventos");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.getContentPane().setBackground(Color.WHITE);
	}
	
	private void init() {
		colocarPaneles();
		colocarEtiqueta();
		colocarBotones();
	}
	
	private void colocarPaneles() {
		panel = new JPanel();
		panel.setLayout(null);
		this.getContentPane().add(panel);
	}
	
	private void colocarEtiqueta() {
		etiqueta = new JLabel("Color(178, 255,18)",SwingConstants.CENTER);
		etiqueta.setBounds(10, 10, 350, 100);
		etiqueta.setOpaque(false);
		panel.add(etiqueta);
	}

	private void colocarBotones() {
		boton = new JButton("R");
		boton.setBounds(40, 200, 80, 80);
		panel.add(boton);
		
		boton1 = new JButton("G");
		boton1.setBounds(140, 200, 80, 80);
		panel.add(boton1);
		
		boton2 = new JButton("B");
		boton2.setBounds(240, 200, 80, 80);
		panel.add(boton2);
		
		interaccionBoton();
		
	}

	private void interaccionBoton() {
		boton.addMouseMotionListener(new MouseMotionListener(){
			@Override
			public void mouseDragged(MouseEvent arg0) {
				panel.setBackground(Color.BLUE);
			}
			@Override
			public void mouseMoved(MouseEvent arg0) {
				panel.setBackground(Color.CYAN);
			}
		});
		boton1.addMouseMotionListener(new MouseMotionListener(){
			@Override
			public void mouseDragged(MouseEvent arg0) {
				panel.setBackground(Color.RED);
			}
			@Override
			public void mouseMoved(MouseEvent arg0) {
				panel.setBackground(Color.YELLOW);
			}
		});
		boton2.addMouseMotionListener(new MouseMotionListener(){
			@Override
			public void mouseDragged(MouseEvent arg0) {
				panel.setBackground(Color.GREEN);
			}
			@Override
			public void mouseMoved(MouseEvent arg0) {
				panel.setBackground(Color.BLACK);
			}
		});
	}

	public static void main(String[] args) {
		Eventos e =new Eventos();
		e.setVisible(true);
	}

}
